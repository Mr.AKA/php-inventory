<?php ob_start();
  $page_title = 'All Product';
  require_once('includes/load.php');
  // Checkin What level user has permission to view this page
   page_require_level(2);
  $products = join_product_table();
?>
<?php include_once('layouts/header.php'); ?>
  <div class="row">
     <div class="col-md-12">
       <?php echo display_msg($msg); ?>
     </div>
    <div class="col-md-12">
      <div class="panel panel-default">
      <div class="panel-heading clearfix">
         <div class="pullt-left col-sm-5">
         <form class="clearfix" method="post" action="sale_report_process.php">
            <div class="form-group">
              
                <div class="input-group">
                  <input type="text" class="datepicker form-control" name="start-date" placeholder="From">
                  <span class="input-group-addon" style="border: none;"><i class="glyphicon glyphicon-menu-right"></i></span>
                  <input type="text" class="datepicker form-control" name="end-date" placeholder="To">
                  <a href="#" class="input-group-addon">
                   <i class="glyphicon">Generate</i>
                  </a>
                </div>
            </div>
            
          </form>
         </div>
         <div class="pull-right col-sm-3">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" class="form-control" name="product-title" placeholder="Product Title">
                  <a href="#" class="input-group-addon">
                   <i class="glyphicon glyphicon-search"> Search </i>
                  </a>
               </div>
               
              </div>
         </div>
         <div class="pull-right col-sm-2">
          <div class="form-group">
                    <select class="form-control" name="product-categorie">
                        <option value="">Select Product Category</option>
                      <?php  foreach ($all_categories as $cat): ?>
                        <option value="<?php echo (int)$cat['id'] ?>">
                          <?php echo $cat['name'] ?></option>
                      <?php endforeach; ?>
                      </select>
                  </div>
          </div>
        </div>
        <div class="panel-body">
          <table class="table table-bordered">
            <thead>
              <tr>
                <th class="text-center" style="width: 50px;">#</th>
                <th> Photo</th>
                <th> Product Title </th>
                <th class="text-center" > Categorie </th>
                <th class="text-center" > Import </th>
                <th class="text-center" > Export </th>
                <th class="text-center" > Broken </th>
                <th class="text-center" > Instock </th>
                <th class="text-center" > last update </th>
                <th class="text-center" > Actions </th>
                
              </tr>
            </thead>
            <tbody >
              <?php foreach ($products as $product):?>
              <tr >
                <td class="text-center"><?php echo count_id();?></td>
                <td>
                  <?php if($product['media_id'] === '0'): ?>
                    <img class="img-avatar img-circle" src="uploads/products/no_image.jpg" alt="">
                  <?php else: ?>
                  <img class="img-avatar img-circle" src="uploads/products/<?php echo $product['image']; ?>" alt="">
                <?php endif; ?>
                </td>
                <td > <?php echo remove_junk($product['name']); ?></td>
                <td class="text-center"> <?php echo remove_junk($product['categorie']); ?></td>
                <td class="text-center text-success"> <?php echo remove_junk($product['quantity']); ?></td>
                <td class="text-center text-warning"> <?php echo remove_junk($product['quantity']); ?></td>
                <td class="text-center text-danger"> <?php echo remove_junk($product['quantity']); ?></td>
                <td class="text-center text-danger"> <?php echo remove_junk($product['quantity']); ?></td>
                <td class="text-center"> <?php echo read_date($product['date']); ?></td>
              
                <td class="text-center" >
                  <div class="btn-group">
                    <a href="import_product.php?id=<?php echo (int)$product['id'];?>" class="float-left">
                      <button type="button" class="btn btn-success btn-xs"  title="Edit" data-toggle="tooltip">Import</button>  
                    </a>
                    <a href="delete_product.php?id=<?php echo (int)$product['id'];?>" class="float-left">
                      <button type="button" class="btn btn-warning btn-xs"  title="Delete" data-toggle="tooltip">Export</button>
                    </a>
                    <a href="delete_product.php?id=<?php echo (int)$product['id'];?>" class="float-left">
                      <button type="button" class="btn btn-danger btn-xs"  title="Delete" data-toggle="tooltip">Broken</button>
                    </a>
                    <a href="delete_product.php?id=<?php echo (int)$product['id'];?>" class="float-left">
                      <button type="button" class="btn btn-info btn-xs"  title="Delete" data-toggle="tooltip">Transection</button>
                    </a>
                  </div>
                
                </td>
                
              </tr>
             <?php endforeach; ?>
            </tbody>
          </tabel>
        </div>
      </div>
    </div>
  </div>
  <?php include_once('layouts/footer.php'); ?>
